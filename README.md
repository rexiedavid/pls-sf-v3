Video URL :https://www.youtube.com/watch?v=HVsySz-h9r4

USEFUL GIT / Windows Commands

git clone url destinationpath
    clone remote repo to local
    
git config -l 
    show list of config

git help [config]
    open browser for help page [config]
git [config] -help 

git init
    initialize a repository from existing code

rm -rf .git
    stop tracking project/ remove .git file

ls -la
    show current directory contents

git status 
    show current git local status, show untracked files

touch .gitignore
    create gitignore file - use to untrack a specific file 

    inside .gitignore file
        can use wildcard '*' to untrack multiple files

git add -A
    add ALL files from the ENTIRE Working Tree to Staging area 

    add -A mydir/ 
        add files from mydir/ to Staging area

    add -u 
        add ONLY UPDATED and DELETED but not UNTRACKED Files from the ENTIRE Working Tree to Staging area
    
    add .
        Works the same with -A as long as you are in your top directory

    add * 
        Not recommended, cannot add deleted history etc

git add [filename]
    add specific file to staging area

git commit
    --amend -m "Correct message"
        To change the commit message

git reset 
    remove fileds to staging area
git reset [filename]
    remove specific to staging area

git remote
    "remote" is the cloud repository

    add origin [url]
    #set a new remote

    -v
    #Verify new remote
    origin  [url] (fetch)
    origin  [url] (push)

git branch
    -a 
    list all branch on both local and remote repo

git diff 
    show changes made to the local code

git pull
    origin master
        pull changes from remote repository master branch

git push 
    origin master
        push changes from local repository master branch to remote repository master branch
    
    u origin [branchname] associate local branch to remote branch

git branch - show all branches
    [name] 
        create a branch
    create branch

git cherry-pick [hash] 
    Used when commit to wrong branch like master

git reset 
    [hash]
        Reset the commit but the files stays at the Working directory

    --hard [hash]
        Reset back to the [hash] copy and delete the updates

    --soft [hash]
        Allows us to reset the working copy (Remove the changes) but the commit will stay in the Staging area
    
git checkout 
    [filename]
        Undo all untracked changes

    git checkout -- .
        Reset to original state/from stash

git clean
    remove/delete untracked files
    
    -df d:untracked directory f: untracked files

git reflog
    shows commits in order of when you last referenced them
    helpful when you accidentaly deleted commits

git revert
    created new commit to reverse the effect of earlier commits, not going to modify existing commits

git stash
    save "message here"
        record the current state of the working directory and the index, but want to go back to a clean working directory. 
        The command saves your local modifications away and reverts the working directory to match the HEAD commit.
    list 
        shows the list of stashed

    apply [stash@{0}]

    pop
        apply the stash and remove the stash in the list

    drop [stash@{0}]

    clear
        get rid of all stashed you have


---------------

Git Stages
    Working Directory > Staging Directory > .git directory (Repository)

#Task001 Rex David 17-01-2020

Return a+b

From sharks to dolphin

<!-- Ice cream is sweet -->